# README #

Required software:

* A Maven-compatible IDE

* Java 8

### What is this repository for? ###

Serves as the repository for my work on a Software Engineering project for school. It's a messy, buggy, inefficient piece of software that serves as my first foray into the realm of web development (both front-end and back-end). It was completed in a little under three days to meet a testing deadline, and I have little interest to go back and fix up the problems.

### Are you ever going to do anything with it? ###

No. I am not interested in web development in the slightest. This was for a class. I have it up and public so that I can always remind myself of how little I enjoyed this and as a warning to others that this is the quality of work you get when you push someone who has no idea what they're doing and give them no solid requirements.

### How do I get set up? ###

Import the project using the provided pom.xml file. Required dependencies will be downloaded. Then simply run the Main.java file in the /src/java/launch/ folder. But I don't know why you'd want to.