<%-- 
    Document   : wishlist.jsp
    Author     : Ian Herbig
--%>

<%@page import="com.project.controllers.UserController"%>
<%@page import="com.project.classes.User"%>
<%@page import="com.project.classes.Item"%>
<%@page import="com.project.classes.WishList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="scripts/jquery-1.10.2.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Wish List</title>
    <h2 align="center">Welcome to the Wish List!</h2>
</head>
<body>
    <%
        UserController userController = UserController.getInstance();
        User user = null;

        try
        {
            user = userController.getOnlineUser(session.getId());
        }
        catch(Exception e)
        {
        }%>

    <%if(user == null)
            {%>
    <p align="center">You must be logged in to use this feature.</p>
    <p align="center">How'd you even get here!?</p>
    <%}
    else
    {
        ArrayList<Item> itemList = Item.getAllItems();
        String [] usersWishListString = WishList.getWishList(user.getName()).split(",") ;
        ArrayList<String> wishList = new ArrayList<String>() ;
        
        for(String item : usersWishListString)
        {
            wishList.add(item) ;
        }
    %>
    <div align="center">
        <div style="display:inline-block;width:49%;float:left">
            <h3 align="center">Wish List</h3>
            <div id="wishlist" align="center">
                <%for(int i = 0 ; i < usersWishListString.length ; i++)
                {%>
                <div class="item" style="width:100%">
                    <p class="description" style="display:inline-block">
                        <%=usersWishListString[i]%>
                    </p>
                    <input class="itemButton1" type="button" style="display:inline-block" value="-">
                </div>
                <%}%>
            </div>
            <input id="save" type="submit" align="right" value="Save">
        </div>
        <div id="items" style="display:inline-block;width:49%">
            <%for(int i = 0 ; i < Item.NUM_ITEMS ; i++)
            {%>
            <div class="item" style="width:100%">
                <p class="description" style="display:inline-block">
                    <%String item = itemList.get(i).toString() ;
                      
                    if(!wishList.contains(item))
                    {%>
                        <%=item%></p>
                    <%}%>
                <input class="itemButton2" type="button" style="display:inline-block" value="+">
            </div>
            <%}%>
        </div>
    </div>
    <script>
        $('.itemButton1').click(function()
        {
            $(this).unbind().parent().appendTo('#items') ;
            $(this).prop('value', '-') ;
            $(this).click(function()
            {
                $(this).prop('value', '+').parent().appendTo('#wishlist') ;
            }) ;
        }) ;
        
        $('.itemButton2').click(function()
        {
            $(this).unbind().parent().appendTo('#wishlist') ;
            $(this).prop('value', '-') ;
            $(this).click(function()
            {
                $(this).prop('value', '+').parent().appendTo('#items') ;
            });
        });

        $('#save').click(function()
        {
            var items = '';
            var size = $('#wishlist .description').size();

            $('#wishlist .description').each(function()
            {
                if (size > 1)
                {
                    items += $(this).text().trim() + ',';
                    size--;
                }
                else
                    items += $(this).text().trim() ;
            });

            var json = {items: items,
                        user: "<%=user.getName()%>"} ;

            $.ajax(
                    {
                        url: '/save',
                        type: "POST",
                        data: json,
                        dataType: 'json',
                        success: function(data)
                        {
                            if (data === 1)
                            {
                                alert('Wishlist saved!');
                            }
                            else
                            {
                                alert('Could not save the wishlist, sorry.');
                            }
                        },
                        error: function(data)
                        {
                            alert('Could not save the wishlist, sorry.');
                        }
                    });
        });
    </script>
    <%}%>
</body>
</html>
