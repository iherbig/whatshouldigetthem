<%-- 
    Document   : changepassword.jsp
    Author     : Ian Herbig
--%>

<%@page import="com.project.controllers.UserController"%>
<%@page import="com.project.classes.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>What Should I Get Them?</title>
    <h1 align="center">Welcome to What Should I Get Them!</h1>
    <%
        UserController userController = UserController.getInstance();
        User user = null;

        try
        {
            user = userController.getOnlineUser(session.getId());
        }
        catch(Exception e){}
    %>
    <%if(user == null)
        {%>
    <p align="center">You must be logged in to use this feature.</p>
    <p align="center">How'd you even get here!?</p>
    <%}
    else
    {%>
        <div align="center" style="margin-top:10%">
            <form action="/changepassword" method="post">
                <p>Please enter current password:</p>
                <input type="password" name="password" placeholder="current password">
                <p>Please enter new password:</p>
                <input type="password" name="newpassword" placeholder="new password">
                <p>Please re-enter new password:</p>
                <input type="password" name="renewpassword" placeholder="new password">
                <p></p>
                <input type="submit" value="Change Password">
            </form>
        </div>
    <%}%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>

