<%-- 
    Document   : register.jsp
    Author     : Ian Herbig
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration Page</title>
    </head>
    <body>
        <div align="center" style="margin-top:10%">
            <form action="/register" method="post">
                <p>Please enter a username:</p>
                <input type="text" name="username" placeholder="username">
                <p>Please enter a password:</p>
                <input type="password" name="password" placeholder="password">
                <p>Re-enter password:</p>
                <input type="password" name="repassword" placeholder="password">
                <p>Please enter e-mail:</p>
                <input type="email" name="email" placeholder="name@example.com">
                <p></p>
                <input type="submit" value="Register">
            </form>
        </div>
    </body>
</html>
