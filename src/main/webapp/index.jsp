<%--
    Document   : index
    Created on : Jul 27, 2014, 3:58:14 PM
    Author     : Ian Herbig
--%>

<%@page import="com.project.controllers.UserController"%>
<%@page import="com.project.classes.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>
        What Should I Get Them?
    </title>
    <h1 align="center">Welcome to What Should I Get Them!</h1>
    <%
        UserController userController = UserController.getInstance();
        User user = null ;

        try
        {
            user = userController.getOnlineUser(session.getId()) ;
        }
        catch(Exception e){}
    %>
    <%if(user == null)
    {%>
    <div align="right" style="display:inline">
        <form action="/login" method="post">
            <input type="text" name="username" placeholder="username">
            <input type="password" name="password" placeholder="password">
            <input type="submit" value="Login">
        </form>
        <p><a href="register.jsp">Register</a></p>
    </div>
    <%}
    else
    {%>
    <div align="right">
        <p>Welcome, <%=user.getName()%>! 
            <a href="/wishlist.jsp?id=<%=user.getName()%>">Wish List</a>
            <a href="/logout">Logout</a>
            <a href="/changepassword.jsp">Change Password</a>
        </p>
    </div>
    <%}%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <div align="center" style="margin-top:10%">
        <p align="center">Here at "What Should I Get Them?" we try our best
            to help you get something for a friend.</p>
        <p align="center">You can either pick things from a list of items that
            your friend likes.</p>
        <p align="center">Or you can answer a bunch of questions about your friend.</p>
        <p align="center">But if you're feeling <em>really</em> lazy, we can
            just give you a random gift.</p>
        <input type="button" onclick="window.location = '/list.jsp';"
               value="Let me list things!" style="display:inline-block">
        <form action="/random" method="post" style="display:inline-block">
            <input type="submit" value="Give me something! Give me anything!">
        </form>
        <input type="submit" onclick="window.location = '/questionnaire.jsp';"
               value="I'll answer some questions!" style="display:inline-block">
    </div>
</body>

