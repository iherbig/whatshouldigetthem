<%-- 
    Document   : list.jsp
    Author     : Ian Herbig
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="scripts/jquery-1.10.2.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Making a List, Checking It Twice</title>
    </head>
    <body>
        <p align="center" style="font-size:16px">
            Please fill out this list of items that have been<br /> 
            conveniently fit into different categories for you to choose from!
        </p>
        <div>
            <fieldset>
                <legend style="font-size:24px">Books</legend>
                <input type="checkbox" name="books" value="1" />"One Hundred Years of Solitude"<br />
                <input type="checkbox" name="books" value="1" />"The Hobbit"<br />
                <input type="checkbox" name="books" value="1" />"The Catcher in the Rye"<br />
                <input type="checkbox" name="books" value="1" />"Harry Potter and the Prisoner of Azkaban"<br />
                <input type="checkbox" name="books" value="1" />"Fahrenheit 451"<br />
                <input type="checkbox" name="books" value="1" />"Lord of the Flies"<br />
                <input type="checkbox" name="books" value="1" />"The Chronicles of Narnia"<br />
                <input type="checkbox" name="books" value="1" />"The Hunger Games"<br />
                <input type="checkbox" name="books" value="1" />"Game of Thrones"<br />
                <input type="checkbox" name="books" value="1" />"Wicked"<br />
            </fieldset>
            <br>
            <fieldset>
                <legend style="font-size:24px">Movies</legend>
                <input type="checkbox" name="movies" value="2" />"The Shawshank Redemption"<br />
                <input type="checkbox" name="movies" value="2" />"Forrest Gump"<br />
                <input type="checkbox" name="movies" value="2" />"Star Wars: A New Hope"<br />
                <input type="checkbox" name="movies" value="2" />"Amadeus"<br />
                <input type="checkbox" name="movies" value="2" />"The Lord of the Rings: The Return of the King"<br />
                <input type="checkbox" name="movies" value="2" />"Raiders of the Lost Ark"<br />
                <input type="checkbox" name="movies" value="2" />"Rocky III"<br />
                <input type="checkbox" name="movies" value="2" />"Pulp Fiction"<br />
                <input type="checkbox" name="movies" value="2" />"Anchorman: The Legend of Ron Burgundy"<br />
                <input type="checkbox" name="movies" value="2" />"Shaun of the Dead"<br />
            </fieldset>
            <br>
            <fieldset>
                <legend style="font-size:24px">Video Games</legend>
                <input type="checkbox" name="Vgames" value="3" />"Final Fantasy VI"<br />
                <input type="checkbox" name="Vgames" value="3" />"Gears of War"<br />
                <input type="checkbox" name="Vgames" value="3" />"Halo 4"<br />
                <input type="checkbox" name="Vgames" value="3" />"Metal Gear Solid: Guns of the Patriots"<br />
                <input type="checkbox" name="Vgames" value="3" />"World of Warcraft: Warlords of Draenor"<br />
                <input type="checkbox" name="Vgames" value="3" />"Super Mario 3D World"<br />
                <input type="checkbox" name="Vgames" value="3" />"Madden NFL 2015"<br />
                <input type="checkbox" name="Vgames" value="3" />"Super Smash Brothers Wii-U"<br />
                <input type="checkbox" name="Vgames" value="3" />"Starcraft II: Heart of the Swarm"<br />
                <input type="checkbox" name="Vgames" value="3" />"Counterstrike: Global Offensive"<br />
            </fieldset>
            <br>
            <fieldset>
                <legend style="font-size:24px">Sports</legend>
                <input type="checkbox" name="Sports" value="4" />"Football"<br />
                <input type="checkbox" name="Sports" value="4" />"Baseball"<br />
                <input type="checkbox" name="Sports" value="4" />"Basketball Jersey"<br />
                <input type="checkbox" name="Sports" value="4" />"Surfboard"<br />
                <input type="checkbox" name="Sports" value="4" />"Bicycle"<br />
                <input type="checkbox" name="Sports" value="4" />"Football Jersey"<br />
                <input type="checkbox" name="Sports" value="4" />"Basketball"<br />
                <input type="checkbox" name="Sports" value="4" />"Tennis Racket"<br />
                <input type="checkbox" name="Sports" value="4" />"ESPN Magazine Subscription"<br />
                <input type="checkbox" name="Sports" value="4" />"Best of 2004 Olympics Blu-Ray"<br />
            </fieldset>
            <br>
            <fieldset>
                <legend style="font-size:24px">Cars</legend>
                <input type="checkbox" name="Cars" value="5" />"Car DVD Player"<br />
                <input type="checkbox" name="Cars" value="5" />"New Car Stereo"<br />
                <input type="checkbox" name="Cars" value="5" />"Yearly Subscription to "Automobile" Magazine"<br />
                <input type="checkbox" name="Cars" value="5" />"A Mercedes"<br />
                <input type="checkbox" name="Cars" value="5" />"A new paint job"<br />
                <input type="checkbox" name="Cars" value="5" />"Steering Wheel Cover"<br />
                <input type="checkbox" name="Cars" value="5" />"Seat Covers"<br />
                <input type="checkbox" name="Cars" value="5" />"Car Vacuum"<br />
                <input type="checkbox" name="Cars" value="5" />"Tire Gauge"<br />
                <input type="checkbox" name="Cars" value="5" />"Emergency Window Breaker"<br />
            </fieldset>
            <br>
            <fieldset>
                <legend style="font-size:24px">Toys</legend>
                <input type="checkbox" name="Toys" value="6" />"Lego Batman Play set"<br />
                <input type="checkbox" name="Toys" value="6" />"Hulk Hands"<br />
                <input type="checkbox" name="Toys" value="6" />"Nerf Watergun"<br />
                <input type="checkbox" name="Toys" value="6" />"A Pokedex"<br />
                <input type="checkbox" name="Toys" value="6" />"Power Ranger Robots"<br />
                <input type="checkbox" name="Toys" value="6" />"Rapunzel Toy House"<br />
                <input type="checkbox" name="Toys" value="6" />"Buzz Lightyear Action Figure"<br />
                <input type="checkbox" name="Toys" value="6" />"A Slinky Dog"<br />
                <input type="checkbox" name="Toys" value="6" />"Mr. Potato head"<br />
                <input type="checkbox" name="Toys" value="6" />"Science Kit"<br />
            </fieldset>
            <br>
            <fieldset>
                <legend style="font-size:24px">Clothing</legend>
                <input type="checkbox" name="Clothing" value="7" />"Football Jersey"<br />
                <input type="checkbox" name="Clothing" value="7" />"Basketball Jersey"<br />
                <input type="checkbox" name="Clothing" value="7" />"Guess Hat"<br />
                <input type="checkbox" name="Clothing" value="7" />"Tank-tops"<br />
                <input type="checkbox" name="Clothing" value="7" />"Vermuda Shorts"<br />
                <input type="checkbox" name="Clothing" value="7" />"Cardigans"<br />
                <input type="checkbox" name="Clothing" value="7" />"Hoodies"<br />
                <input type="checkbox" name="Clothing" value="7" />"Scarves"<br />
                <input type="checkbox" name="Clothing" value="7" />"Sleeveless Shirt"<br />
                <input type="checkbox" name="Clothing" value="7" />"Guess Jeans"<br />
            </fieldset>
            <br>
            <fieldset>
                <legend style="font-size:24px">Jewelry</legend>
                <input type="checkbox" name="Jewelry" value="8" />"Teardrop Earrings"<br />
                <input type="checkbox" name="Jewelry" value="8" />"Bangles"<br />
                <input type="checkbox" name="Jewelry" value="8" />"Bracelets"<br />
                <input type="checkbox" name="Jewelry" value="8" />"Ring"<br />
                <input type="checkbox" name="Jewelry" value="8" />"Ankle Bracelet"<br />
                <input type="checkbox" name="Jewelry" value="8" />"Lip Ring"<br />
                <input type="checkbox" name="Jewelry" value="8" />"Nose Ring"<br />
                <input type="checkbox" name="Jewelry" value="8" />"Tiara"<br />
                <input type="checkbox" name="Jewelry" value="8" />"Diamond Ring"<br />
                <input type="checkbox" name="Jewelry" value="8" />"Pendant Necklaces"<br />
            </fieldset>
            <br>
            <fieldset>
                <legend style="font-size:24px">Outdoors</legend>
                <input type="checkbox" name="Outdoors" value="9" />"Swiss Army Knife"<br />
                <input type="checkbox" name="Outdoors" value="9" />"Portable Propane"<br />
                <input type="checkbox" name="Outdoors" value="9" />"Cooler Organizer"<br />
                <input type="checkbox" name="Outdoors" value="9" />"Portable Campfire"<br />
                <input type="checkbox" name="Outdoors" value="9" />"Tent"<br />
                <input type="checkbox" name="Outdoors" value="9" />"Rugged XL Lantern"<br />
                <input type="checkbox" name="Outdoors" value="9" />"Folding Camp Shovel"<br />
                <input type="checkbox" name="Outdoors" value="9" />"LED Headlamp"<br />
                <input type="checkbox" name="Outdoors" value="9" />"Camp Axe"<br />
                <input type="checkbox" name="Outdoors" value="9" />"Emergency Blanket"<br />
            </fieldset>
            <br>
            <fieldset>
                <legend style="font-size:24px">Entertainment Hardware</legend>
                <input type="checkbox" name="EHardware" value="10" />"Car DVD Player"<br />
                <input type="checkbox" name="EHardware" value="10" />"Xbox One"<br />
                <input type="checkbox" name="EHardware" value="10" />"PlayStation 4"<br />
                <input type="checkbox" name="EHardware" value="10" />"Wii-U"<br />
                <input type="checkbox" name="EHardware" value="10" />"Nintendo 3DS"<br />
                <input type="checkbox" name="EHardware" value="10" />"Playstation Vita"<br />
                <input type="checkbox" name="EHardware" value="10" />"Blu-Ray Player"<br />
                <input type="checkbox" name="EHardware" value="10" />"GTX 760Ti Video Card"<br />
                <input type="checkbox" name="EHardware" value="10" />"Wireless Headphones"<br />
                <input type="checkbox" name="EHardware" value="10" />"iPad 3"<br />
            </fieldset>
            <br>
            <input id="submitButton" type="submit" value="Recommend something to me!" />
        </div>
        <script>
            $('#submitButton').click(function()
            {
                var selections = "" ;
                var count = 0 ;
                
                $("input:checkbox").each(function()
                {
                    if($(this).prop("checked"))
                    {
                        if(count < 99)
                        {
                            selections += $(this).prop("value") + "," ;
                        }
                        else
                            selections += $(this).prop("value") ;
                        
                        count++ ;
                    }
                }) ;
                
                var json = {categoryList: selections} ;
                
                $.ajax(
                {
                    url: '/list',
                    type: "POST",
                    data: json,
                    dataType: 'json',
                    success: function(data)
                    {
                        window.location = data.url ;
                    },
                    error: function(data)
                    {
                        alert('There was a problem. Sorry.');
                    }
                });
            }) ;
        </script>
    </body>