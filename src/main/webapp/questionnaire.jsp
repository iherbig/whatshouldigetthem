<%-- 
    Document   : questionnaire.jsp
    Author     : Ian Herbig
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="scripts/jquery-1.10.2.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Questionnaire</title>
    <h1 align="center">Welcome to the Questionnaire!</h1>
    <p align ="center">Select the answers you feel most accurately describe your friend.<br />
        We'll try to give you some cool recommendations that you can gift them!</p>
</head>
<body>
    <div id="questionnaire" style="width:45%;margin:0 auto" align="center">
        <div style="font-size:14px" align="left">
            <p style="font-size:24px" align="center">Question 1</p>
            <div class="answers1" align="left" style="width:45%;margin:0 auto">
                <p style="font-size:18px">Does your friend enjoy spending time outside?</p>
                <input type="radio" name="a1" value="9">Yes<br />
                <input type="radio" name="a1" value="0">No<br />
                <input type="radio" name="a1" value="0">I really have no idea
            </div>
        </div>
        <div style="font-size:14px" align="left">
            <p style="font-size:24px" align="center">Question 2</p>
            <div class="answers2" style="width:45%;margin:0 auto" align="left">
                <p style="font-size:18px">Are they a sports fan?</p>
                <input type="radio" name="a2" value="4">Yes<br />
                <input type="radio" name="a2" value="0">No<br />
                <input type="radio" name="a2" value="0">I'm not sure
            </div>
        </div>
        <div style="font-size:14px" align="left">
            <p style="font-size:24px" align="center">Question 3</p>
            <div class="answers3" style="width:45%;margin:0 auto" align="left">
                <p style="font-size:18px">Are they more likely to buy new 
                    parts for their PC or their car?</p>
                <input type="radio" name="a3" value="10">PC, definitely<br />
                <input type="radio" name="a3" value="5">They love their car
            </div>
        </div>
        <div style="font-size:14px" align="left">
            <p style="font-size:24px" align="center">Question 4</p>
            <div class="answers4" style="width:45%;margin:0 auto" align="left">
                <p style="font-size:18px">Do they enjoy electronics?</p>
                <input type="radio" name="a4" value="10">Yes<br />
                <input type="radio" name="a4" value="3">Really just video games<br />
                <input type="radio" name="a4" value="0">No<br />
                <input type="radio" name="a4" value="6">They're more into toys than electronics<br />
                <input type="radio" name="a4" value="0">I'm not sure
            </div>
        </div>
        <div style="font-size:14px" align="left">
            <p style="font-size:24px" align="center">Question 5</p>
            <div class="answers5" style="width:45%;margin:0 auto" align="left">
                <p style="font-size:18px">Are they a movie buff?</p>
                <input type="radio" name="a5" value="2">Yes<br />
                <input type="radio" name="a5" value="0">No
            </div>
        </div>
        <div style="font-size:14px" align="left">
            <p style="font-size:24px" align="center">Question 6</p>
            <div class="answers6" style="width:45%;margin:0 auto" align="left">
                <p style="font-size:18px">Do they enjoy books?</p>
                <input type="radio" name="a6" value="1">Yes<br />
                <input type="radio" name="a6" value="0">No
            </div>
        </div>
        <div style="font-size:14px" align="left">
            <p style="font-size:24px" align="center">Question 7</p>
            <div class="answers7" style="width:45%;margin:0 auto" align="left">
                <p style="font-size:18px">Are they into accessorizing?</p>
                <input type="radio" name="a7" value="8">Yes, they love jewelry.<br />
                <input type="radio" name="a7" value="7">They're more about clothes than jewelry or accessories.
            </div>    
        </div>
        <p />
        <input id="submitButton" type="submit" value="Recommend something to me!" />
    </div>
    <script>
        $('#submitButton').click(function()
        {
            var all_answered = true ;
            
            $("input:radio").each(function()
            {
                var name = $(this).prop("name") ;
                if($("input:radio[name=" + name + "]:checked").length === 0)
                {
                    all_answered = false ;
                }
            }) ;
            
            if(!all_answered)
            {
                alert("You need to answer all of the questions.") ;
            }
            else
            {
                var answers = "" ;
                
                for(var i = 1 ; i < 8 ; i++)
                {
                    if(i < 7)
                    {
                        answers += $('input[name=a' + i + ']:checked').val() + ',' ;
                    }
                    else
                    {
                        answers += $('input[name=a' + i + ']:checked').val() ;
                    }
                }
                
                var json = {answers: answers} ;
                
                $.ajax(
                {
                    url: '/questionnaire',
                    type: "POST",
                    data: json,
                    dataType: 'json',
                    success: function(data)
                    {
                        window.location = data.url ;
                    },
                    error: function(data)
                    {
                        alert('There was an error. Sorry.');
                    }
                });
            }
        });
    </script>
</body>