<%-- 
    Document   : recommendation.jsp
    Author     : Ian Herbig
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Here It Is!</title>
    </head>
    <body>
        <h1 align="center">You should get them: <%=request.getParameter("itemname")%>!</h1>
        <form action="/random" method="post" align="center">
            <input type="submit" value="Give me something else!">
        </form>
    </body>
</html>
