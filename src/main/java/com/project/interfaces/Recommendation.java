package com.project.interfaces;

import com.project.classes.Item;

/**
 * File: Recommendation.java
 * Description: This file specifies the interface which each form of
 * recommendation must implement.
 *
 * @author iherb001
 */
public interface Recommendation
{
    public Item generateRecommendation() ;
}
