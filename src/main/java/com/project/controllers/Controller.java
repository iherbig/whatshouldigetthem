package com.project.controllers;

import com.project.classes.Item;
import com.project.classes.User;
import com.project.classes.WishList;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * File:Controller.java
 * Description: The servlet which handles all POST and GET requests
 * sent by the browser.
 *
 * @author iherb001
 */
@WebServlet(name = "Controller",
            urlPatterns =
            {
                "/Controller",
                "/login",
                "/logout",
                "/register",
                "/list",
                "/questionnaire",
                "/random",
                "/wishlist",
                "/save",
                "/changepassword"
            })
public class Controller extends HttpServlet
{

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * No GET methods are handled. All incoming requests are POST requests (mostly due to
     * lack of knowledge). All incoming requests are forwarded through here, relying on
     * a parameter passed in with the request to determine exactly what to do.
     *
     * The parameters are either processed and forwarded or directly forwarded to
     * the appropriate controller. Depending on the request, the servlet may either
     * return a JSON object featuring the data to be displayed to the user or
     * simply redirect the user to the appropriate page.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");

        String requestType = request.getServletPath(); // determine the type of request

        /*
         * Then we want to check if a user is logged in. We utilize the static
         * User Controller, which associates session IDs with logged in users.
         * Some requests require a username, and in the case of those requests
         * the variable "user" will point to the User object representing the
         * logged in user.
         */
        UserController userController = UserController.getInstance();
        User user = null ;
        
        try
        {
            user = userController.getOnlineUser(request.getSession().getId()) ;
        }
        catch(Exception e){}

        //for generating new pages from the request, or providing a simple response
        PrintWriter out = response.getWriter();

        /*
         * If the user is trying to log in, we provide provide the userController instance
         * with the login parameters (the current session ID, the username the user is trying
         * to log in with, and the password the user is trying to log in with. A simple boolean
         * would not suffice since there are three different possibilities when a user attempts
         * to log in.
         *
         * If they succeed, in which case the main page is merely reloaded (logic on that page
         * loads a page that indicates that they are logged in instead of providing with a log
         * in prompt). If they provided the incorrect credentials, we notify the user. And if
         * they are locked out (because of repeated log in failures), they are notified as well.
         */
        if(requestType.equals("/login"))
        {

            int couldLogin = userController.login(request.getSession().getId(),
                                                  request.getParameter("username"),
                                                  request.getParameter("password"));

            if(couldLogin == 0)
            {
                response.sendRedirect("/");
            }
            if(couldLogin == 1)
            {
                failedLogin(out);
            }
            if(couldLogin == 2)
            {
                lockedOut(out);
            }
        }

        /*
         * If the user tries to log out, they are simply removed from the list of users that
         * are logged in, and the main page is reloaded.
         */
        if(requestType.equals("/logout"))
        {
            userController.logout(request.getSession().getId());
            response.sendRedirect("/");
        }

        /*
         * If a user attempts to register, the User Controller is notified and the registration
         * process begins with the information provided by the user. If it succeeds, the user
         * is immediately logged in and they are redirected to the front page. If it fails,
         * then the user is notified.
         */
        if(requestType.equals("/register"))
        {
            String requestedUsername = request.getParameter("username");
            String requestedPassword = request.getParameter("password");
            String email = request.getParameter("email");
            
            if(userController.register(requestedUsername,
                                       requestedPassword,
                                       email))
            {
                userController.loginImmediate(request.getSession().getId(),
                                              requestedUsername);
                response.sendRedirect("/");
            }
            else
            {
                failedRegistration(out);
            }
        }

        /*
         * The list and questionnaire paths are similar to one another.
         * They both accept the user input as a series of comma separated integers.
         * Those integers are then processed as categories and a recommendation
         * is generated from those categories.
         *
         * A response is sent to the user in the form of a JSONObject which contains
         * the URL for the user to be redirected to. URL parameters are utilized
         * in the display of a given page. This is due to these calls coming in
         * as AJAX calls.
         */
        if(requestType.equals("/list"))
        {
            String [] itemStrings = request.getParameter("categoryList").split(",") ;

            RecommendationController recommendationController
                    = new RecommendationController(1) ;
            
            recommendationController.setCategoryList(itemStrings) ;

            Item recommendation = recommendationController.generateRecommendation();

            JSONObject jsonObject = new JSONObject() ;
            try
            {
                jsonObject.put("url", "/recommendation.jsp?itemname=" + recommendation) ;
                out.write(jsonObject.toString()) ;
            }
            catch(JSONException ex)
            {
                System.err.println("Could not put url into JSON object.") ;
                ex.printStackTrace() ;
            }
        }
        if(requestType.equals("/questionnaire"))
        {
            String [] answerStrings = request.getParameter("answers").split(",") ;
            
            RecommendationController recommendationController 
                    = new RecommendationController(2) ;
            
            recommendationController.setCategoryList(answerStrings) ;
            
            Item recommendation = recommendationController.generateRecommendation() ;
            
            JSONObject jsonObject = new JSONObject() ;
            try
            {
                jsonObject.put("url", "/recommendation.jsp?itemname=" + recommendation) ;
                out.write(jsonObject.toString()) ;
            }
            catch(JSONException ex)
            {
                System.err.println("Could not put url into JSON object.") ;
                ex.printStackTrace() ;
            }
        }

        /*
         * If the user tries to get a random recommendation, one is simply pulled from the
         * database and provided to the user using the same URL parameter trick utilized
         * for the list/questionnaire paths, except the user is directly redirected by
         * the servlet (as these aren't asynchronous calls).
         */
        if(requestType.equals("/random"))
        {
            RecommendationController recommendationController
                    = new RecommendationController(0);
            Item recommendation = recommendationController.generateRecommendation();

            response.sendRedirect("/recommendation.jsp?itemname=" + recommendation);
        }

        /*
         * If the user wants to save a wishlist, the servlet handles that here.
         * The items are stored as a comma-separated string of items that
         * the user picked.
         */
        if(requestType.equals("/save"))
        {
            String itemString = request.getParameter("items") ;
            String userName = request.getParameter("user") ;
            
            WishList wishList = new WishList(itemString, userName) ;
            
            if(wishList.saveWishList())
            {
                out.write("1") ;
            }
            else
            {
                out.write("0") ;
            }
        }

        /*
         * Finally, when the user attempts to change their password, this path
         * handles the incoming data and forwards it to the User Controller
         * to handle the actual changing of the password.
         */
        if(requestType.equals("/changepassword"))
        {
            String currentPassword = request.getParameter("password") ;
            String newPassword = request.getParameter("newpassword") ;
            String username = user.getName() ;
            
            if(userController.changePassword(username, currentPassword, newPassword))
            {
                response.sendRedirect("/") ;
            }
            else
            {
                response.sendRedirect("/failure.jsp") ;
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Private helper methods. Click on the + sign on the left to view the code.">
    private void failedRegistration(PrintWriter out)
    {
        out.append("<!doctype html><html>");
        out.append("<head><title>Registration Failed</title></head>");
        out.append("<body><p align=\"center\">Username is taken.</p></body></html>");
    }

    private void failedLogin(PrintWriter out)
    {
        out.append("<!doctype html><html>");
        out.append("<head><title>Login Failed</title></head>");
        out.append("<body><p align=\"center\">Username or password provided"
                + " was incorrect.</p></body></html>");
    }

    private void lockedOut(PrintWriter out)
    {
        out.append("<!doctype html><html>");
        out.append("<head><title>Lockout</title></head>");
        out.append("<body><p align=\"center\">Your account has been locked out."
                + " Try again later.</p></body></html>");
    } // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to view the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Universal logic rerouter.";
    }// </editor-fold>

}
