package com.project.controllers;

import com.project.classes.Category;
import com.project.classes.Item;
import com.project.classes.List;
import com.project.classes.Questionnaire;
import com.project.classes.Random;
import com.project.interfaces.Recommendation;
import java.util.ArrayList;

/**
 * File: RecommendationController.java
 * Description: Creates and calls the appropriate type of recommendation generator based
 * on the user's request.
 *
 * @author iherb001
 */
public class RecommendationController
{
    private ArrayList<Category> categoryList;
    private Recommendation generator ;
    private int type;

    public RecommendationController(int type)
    {
        this.type = type;
    }

    /**
     * Transforms a list of strings into a list of categories.
     * Each string is turned into an integer and then the appropriate
     * category is made that corresponds to that integer. The integer's
     * representation of the category is based on the category's
     * id within the database.
     *
     * @param answerStrings the list of strings
     */
    public void setCategoryList(String [] answerStrings)
    {
        ArrayList<Category> categoryArrayList = new ArrayList<>() ;
        
        for(String category : answerStrings)
        {
            categoryArrayList.add(new Category(Integer.parseInt(category))) ;
        }
        
        this.categoryList = categoryArrayList;
    }

    /**
     * Creates the appropriate type of generator and returns the generated recommendation.
     *
     * @return the generated recommendation
     */
    public Item generateRecommendation()
    {
        if(type == 0)
        {
            generator = new Random() ;
        }
        if(type == 1)
        {
            generator = new List(categoryList);
        }
        if(type == 2)
        {
            generator = new Questionnaire(categoryList);
        }

        return generator.generateRecommendation() ;
    }
}
