package com.project.controllers;

import com.project.classes.Category;
import static com.project.classes.Category.NUM_ITEMS_PER_CATEGORY;
import com.project.classes.Item;
import static com.project.classes.Random.randomNumber;
import com.project.connection.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * File: DatabaseController.java
 * Description: This class handles the connections made to the database
 * and the various queries necessary to retrieve and store data therein.
 *
 * @author iherb001
 */
public class DatabaseController
{

    // <editor-fold defaultstate="collapsed" desc="Various queries necessary through the course of the web application's life">
    private static final String find_user_query 
            = "SELECT username "
            + "FROM users "
            + "WHERE username = ? AND password = ?" ;
    private static final String register_user_query 
            = "INSERT INTO users (username, password, email) "
            + "VALUES (? , ? , ?)" ;
    private static final String get_item_query 
            = "SELECT i_name, i_place "
            + "FROM item "
            + "WHERE category = ? AND i_place = ?" ;
    private static final String get_item_by_name_query 
            = "SELECT i_name, cat_id "
            + "FROM item, categories "
            + "WHERE i_name = ? AND category = cat_name" ;
    private static final String save_wishlist_query
            = "INSERT INTO wishlist (wishlist_id, u_name) "
            + "VALUES (? , ?)" ;
    private static final String update_wishlist_query
            = "UPDATE wishlist "
            + "SET (wishlist_id) = ( ? ) "
            + "WHERE u_name = ?" ;
    private static final String get_wishlist_query
            = "SELECT wishlist_id "
            + "FROM wishlist "
            + "WHERE u_name = ?" ;
    private static final String get_all_items_query
            = "SELECT i_name "
            + "FROM item" ;
    private static final String change_password_query
            = "UPDATE users "
            + "SET (password) = ( ? ) "
            + "WHERE username = ?" ; // </editor-fold>

    // All methods of this class are static, so default constructor used.
    public DatabaseController(){}

    /**
     * Queries the database to check if the credentials provided by the user
     * for a log in are correct.
     *
     * @param sessionid the sessionID of the current user
     * @param username the username the user provided
     * @param password the password the user provided
     * @return whether or not the user could log in
     */
    public static boolean login(String sessionid, String username, String password)
    {
        Connection connection = Database.connect() ;
        PreparedStatement query = null ;
        String encryptedPassword = encryptPass(password) ;
        
        try
        {
            query = connection.prepareStatement(find_user_query) ;
            query.setString(1, username) ;
            query.setString(2, encryptedPassword) ;
            ResultSet queryResults = query.executeQuery() ;
            
            while(queryResults.next())
            {
                return true ;
            }
        }
        catch(SQLException e)
        {
            System.err.println("There was an error finding the user while logging in:") ;
            e.printStackTrace() ;
        }
        finally
        {
            try
            {
                connection.close() ;
                query.close() ;
            }
            catch(SQLException e)
            {
                System.err.println("Failed to close the connection.") ;
            }
        }
        
        return false ;
    }

    /**
     * Inserts a row into the table of users in the database with the requested
     * information. A SQLException occurs if the username requested is already
     * in the database in which case false is returned.
     *
     * @param requestedUsername the username requested
     * @param requestedPassword the password requested
     * @param email the user's email
     * @return whether the registration was successful
     */
    public static boolean register(String requestedUsername,
                               String requestedPassword,
                               String email)
    {
        Connection connection = Database.connect() ;
        PreparedStatement query = null ;
        String encryptedRequestedPassword = encryptPass(requestedPassword) ;
        
        try
        {
            query = connection.prepareStatement(register_user_query) ;
            query.setString(1, requestedUsername.toLowerCase()) ; //username
            query.setString(2, encryptedRequestedPassword) ; //password
            query.setString(3, email.toLowerCase()) ; //email
            query.executeUpdate() ;

            return true ;
        }
        catch(SQLException e)
        {
            return false ;
        }
        finally
        {
            try
            {
                connection.close() ;
                query.close() ;
            }
            catch(SQLException e)
            {
                System.err.println("Failed to close the connection.") ;
            }
        }
    }

    /**
     * Gets an item from the database by its name.
     *
     * @param itemName the name of the item to be retrieved
     * @return the item from the database
     */
    public static Item getItem(String itemName)
    {
        Connection connection = Database.connect() ;
        PreparedStatement query = null ;
        Item recommendation = null ;
        
        try
        {
            query = connection.prepareStatement(get_item_by_name_query) ;
            query.setString(1, itemName) ;
            ResultSet queryResults = query.executeQuery() ;
            
            while(queryResults.next())
            {
                recommendation = new Item(itemName, new Category(queryResults.getInt("cat_id"))) ;
            }
        }
        catch(SQLException e)
        {
            System.err.println("Could not retrieve item from database.") ;
            e.printStackTrace() ;
        }
        finally
        {
            try
            {
                connection.close() ;
                query.close() ;
            }
            catch(SQLException e)
            {
                System.err.println("Could not close connection to database.") ;
            }
        }
        
        return recommendation ;
    }

    /**
     * Gets a random item from the database in a requested category.
     * @param category the category to retrieve the item from
     * @return the item from the database
     */
    public static Item getItem(Category category)
    {
        int categoryNumber = category.getCategoryNumber() ;
        Connection connection = Database.connect() ;
        PreparedStatement query = null ;
        Item recommendation = null ;
        
        try
        {
            query = connection.prepareStatement(get_item_query) ;
            query.setInt(1, categoryNumber) ;
            query.setInt(2, randomNumber(1, NUM_ITEMS_PER_CATEGORY)) ;
            ResultSet queryResults = query.executeQuery() ;
            
            while(queryResults.next())
            {
                recommendation = new Item(queryResults.getString("i_name")) ;
            }
                
        }
        catch(SQLException e)
        {
            System.err.println("There was an error retrieving the item.") ;
            e.printStackTrace() ;
        }
        finally
        {
            try
            {
                connection.close() ;
                query.close() ;
            }
            catch(SQLException e)
            {
                System.err.println("Could not close the connection.") ;
            }
        }
        
        return recommendation ;
    }

    /**
     * Inserts a wishlist into the database, associating it with the user
     * currently logged in. The wishlist is stored as a comma-separated
     * list of item names.
     *
     * @param wishList a comma-separated list of item names
     * @param userName the name of the user to which the wishlist belongs
     * @return whether the insert succeeded
     */
    public static boolean saveWishList(String wishList, String userName)
    {
        Connection connection = Database.connect() ;
        PreparedStatement query = null ;
        boolean saved = false ;
        
        try
        {
            query = connection.prepareStatement(update_wishlist_query) ;
            query.setString(1, wishList) ;
            query.setString(2, userName) ;
            int rowsUpdated = query.executeUpdate() ;
            
            saved = true ;
            
            if(rowsUpdated < 1)
            {
                saved = false ;
                
                query.close() ;
                query = connection.prepareStatement(save_wishlist_query) ;
                query.setString(1, wishList) ;
                query.setString(2, userName) ;
                rowsUpdated = query.executeUpdate() ;
                
                if(rowsUpdated > 0)
                    saved = true ;
            }
        }
        catch(SQLException e)
        {
            System.err.println("There was an error saving the wishlist.") ;
            e.printStackTrace() ;
            saved = false ;
        }
        finally
        {
            try
            {
                query.close() ;
                connection.close() ;
            }
            catch(SQLException e)
            {
                System.err.println("Could not close the connection.") ;
            }
        }
        
        return saved ;
    }

    /**
     * Retrieves a wishlist from the database associated with a given user.
     *
     * @param userName the user who wants their wishlist information
     * @return the wishlist as a comma-separated list of items
     */
    public static String retrieveWishList(String userName)
    {
        String wishList = "" ;
        Connection connection = Database.connect() ;
        PreparedStatement query = null ;
        
        try
        {
            query = connection.prepareStatement(get_wishlist_query) ;
            query.setString(1, userName) ;
            ResultSet queryResults = query.executeQuery() ;
            
            while(queryResults.next())
            {
                wishList = queryResults.getString("wishlist_id") ;
            }
        }
        catch(SQLException e)
        {
            System.err.println("Could not retrieve the wish list.") ;
            e.printStackTrace() ;
        }
        finally
        {
            try
            {
                query.close() ;
                connection.close() ;
            }
            catch(SQLException e)
            {
                System.err.println("Could not close the connection.") ;
            }
        }
        
        return wishList ;
    }

    /**
     * Retrieves ALL items that are in the database. Very inefficient, only
     * usable while there are relatively few items in the database. Used
     * on the wishlist page.
     *
     * @return an ArrayList of all of the items stored in the database
     */
    public static ArrayList<Item> getAllItems()
    {
        ArrayList<Item> itemList = new ArrayList<>() ;
        Connection connection = Database.connect() ;
        PreparedStatement query = null ;
        
        try
        {
            query = connection.prepareStatement(get_all_items_query) ;
            ResultSet queryResults = query.executeQuery() ;
            
            while(queryResults.next())
            {
                itemList.add(new Item(queryResults.getString("i_name"))) ;
            }
        }
        catch(SQLException e)
        {
            System.err.println("Could not retrieve the wish list.") ;
            e.printStackTrace() ;
        }
        finally
        {
            try
            {
                query.close() ;
                connection.close() ;
            }
            catch(SQLException e)
            {
                System.err.println("Could not close the connection.") ;
            }
        }
        
        return itemList ;
    }

    /**
     * Changes a given user's password stored in the database.
     *
     * @param userName the username of the user who wants to change their password
     * @param newPassword the new password
     * @return whether the change was successful
     */
    public static boolean changePassword(String userName, String newPassword)
    {
        Connection connection = Database.connect() ;
        PreparedStatement query = null ;
        
        try
        {
            query = connection.prepareStatement(change_password_query) ;
            query.setString(1, newPassword) ;
            query.setString(2, userName) ;
            int rowsUpdated = query.executeUpdate() ;
            
            if(rowsUpdated < 1)
            {
                return false ;
            }
            else
                return true ;
        }
        catch(SQLException e)
        {
            System.err.println("Could not retrieve the wish list.") ;
            e.printStackTrace() ;
        }
        finally
        {
            try
            {
                query.close() ;
                connection.close() ;
            }
            catch(SQLException e)
            {
                System.err.println("Could not close the connection.") ;
            }
        }
        
        return false ;
    }

    // <editor-fold defaultstate="collapsed" desc="The encryption algorithm for storing passwords"
    private static String encryptPass(String password)
    {
        return password ;
    }// </editor-fold>
}
