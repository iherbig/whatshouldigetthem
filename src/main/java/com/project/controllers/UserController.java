package com.project.controllers;

import com.project.classes.ProfileSecurityManager;
import com.project.classes.User;
import java.util.HashMap;
import java.util.Map;

/**
 * File: UserController.java
 * Description: Handles the login, registration, and change password procedures.
 * In addition, it contains a list of online users, associating session IDs
 * with those users.
 *
 * @author Ian Herbig
 */
public class UserController
{
    private static UserController instance ;
    private static Map<String, User> usersOnline ;
    
    private UserController()
    {
        usersOnline = new HashMap<>() ;
    }

    /**
     * Handles the creation (if necessary) of the Singleton UserController.
     *
     * @return the Singleton instance of the UserController
     */
    public static UserController getInstance()
    {
        if(instance == null)
        {
            instance = new UserController() ;
            return instance ;
        }
        else
            return instance ;
            
    }

    /**
     * Gets the User associated with a given sessionID.
     *
     * @param sessionID the user's session id
     * @return the User associated with the given sessionID
     */
    public User getOnlineUser(String sessionID)
    {
        return usersOnline.get(sessionID) ;
    }

    /**
     * The login handler, which utilizes the ProfileSecurityManager (PSM) object to log
     * attempt to log in a given user. If the PSM gives the UserController permission to
     * log a user in, we attempt to log in by checking credentials stored in the database.
     * If the user's credentials are validated, then the sessionID is mapped to the User
     * and stored. Otherwise, we return a sentinel value representing our failure.
     *
     * @param sessionid the sessionid of the user trying to log in
     * @param username the user's provided username
     * @param password the user's provided password
     * @return a return code representing success or type of failure: 0 on success, 1 on
     * failure to validate credentials, 2 if the user is locked out of their account
     */
    public int login(String sessionid, String username, String password)
    {
        ProfileSecurityManager profileSecurityManager = 
                ProfileSecurityManager.getInstance() ;
        
        if(profileSecurityManager.canLogin(username))
        {
            if(DatabaseController.login(sessionid, username, password))
            {
                usersOnline.put(sessionid, new User(username)) ;
                profileSecurityManager.loggedIn(username) ;
                
                return 0 ;
            }
            else
                return 1 ;       
        }
        else
            return 2 ;
    }

    /**
     * Unassociates the current session id with a User.
     *
     * @param sessionid the user's current session id
     */
    public void logout(String sessionid)
    {
        usersOnline.remove(sessionid) ;
    }

    /**
     * Attempts to register a user with an account.
     *
     * @param requestedUsername the requested username
     * @param requestedPassword the requested password
     * @param email the requested email
     * @return whether the registration was successful
     */
    public boolean register(String requestedUsername,
                            String requestedPassword,
                            String email)
    {
        boolean couldRegister = DatabaseController.register(requestedUsername,
                                                        requestedPassword,
                                                        email) ;
        
        return couldRegister ;
    }

    /**
     * Logs a user in (associates their session id with a User) without
     * database access or credential validation.
     *
     * @param sessionid the sessionid of the user trying to log in
     * @param username the user's provided username
     */
    public void loginImmediate(String sessionid,
                                  String username)
    {
        usersOnline.put(sessionid, new User(username)) ;
    }

    /**
     * Attempts to change a user's password.
     *
     * @param userName the user's provided username
     * @param password the user's provided password
     * @param newPassword the requested new password
     * @return whether the change was successful
     */
    public boolean changePassword(String userName, String password, String newPassword)
    {
        return DatabaseController.changePassword(userName, newPassword) ;
    }
}
