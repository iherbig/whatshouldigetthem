package com.project.classes;

/**
 * File: User.java
 * Description: An object to represent a logged in user.
 *
 * @author Ian Herbig
 */
public class User
{
    private String username ;
    private String email ;

    /**
     * Creates a new User associated with a given name.
     *
     * @param username the username provided
     */
    public User(String username)
    {
        this.username = username ;
    }

    /**
     * Creates a new User associated with a given name and e-mail address.
     *
     * @param username the username provided
     * @param email the e-mail address provided
     */
    public User(String username, String email)
    {
        this.username = username ;
        this.email = email ;
    }

    /**
     * A simple getter for the User's name.
     *
     * @return the User's name
     */
    public String getName()
    {
        return username ;
    }

    /**
     * A simple getter for the User's e-mail.
     *
     * @return the User's e-mail
     */
    public String getEmail()
    {
        return email ;
    }

    /**
     * A simple setter for the User's name.
     *
     * @param username the username provided
     */
    public void setName(String username)
    {
        this.username = username ;
    }

    /**
     * A simple setter for the User's e-mail address.
     *
     * @param email the e-mail address provided
     */
    public void setEmail(String email)
    {
        this.email = email ;
    }
}
