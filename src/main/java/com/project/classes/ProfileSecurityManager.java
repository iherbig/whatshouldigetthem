package com.project.classes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * File: ProfileSecurityManager.java
 * Description: This class exists as a Singleton object. Once an instance is created
 * or retrieved, it then keeps track of all attempts made to log in to a particular
 * user's account and which users have been locked out.
 *
 * @author Ian Herbig
 */
public class ProfileSecurityManager
{
    private static ProfileSecurityManager instance ;
    private static Map<String, ArrayList<String>> userLoginTimes ;
    private static ArrayList<String> lockedOutUsers ;
    
    private ProfileSecurityManager()
    {
        userLoginTimes = new HashMap<>() ;
        lockedOutUsers = new ArrayList<>() ;
    }

    /**
     * Singleton design pattern. Returns instance if it exists or creates a new one if it doesn't.
     *
     * @return the instance of the object
     */
    public static ProfileSecurityManager getInstance()
    {
        if(instance == null)
        {
            instance = new ProfileSecurityManager() ;
            return instance ;
        }
        else
            return instance ;
    }

    /**
     * This method will check if a given User can log in.
     *
     * Whether a user can log in is decided by the following algorithm:
     * 1. If the User's account is locked, they cannot log in.
     * 2. If the User's account has three logged login attempts, they are locked out.
     * 3. If neither of the above two are true, then they can log in and the attempt is registered.
     *
     * @param username the username of the user attempting to log in
     * @return whether that user can log in or not
     */
    public boolean canLogin(String username)
    {
        if(lockedOutUsers.contains(username))
            return false ;
        else
        {
            ArrayList<String> loginTimes = userLoginTimes.get(username) ;
            
            if(loginTimes != null)
            {
                if(loginTimes.size() > 2)
                {
                    lockedOutUsers.add(username) ;
                    return false ;
                }
                else
                {
                    userLoginTimes.get(username).add(new SimpleDateFormat("yyyyMMdd_HHmmss")
                            .format(Calendar.getInstance().getTime())) ;
                    
                    return true ;
                }
            }
            
            userLoginTimes.put(username, new ArrayList<>()) ;
            userLoginTimes.get(username).add(new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(Calendar.getInstance().getTime())) ;
            
            return true ;
        }
    }

    /**
     * Once a user is logged in, the log of their login times is deleted to prevent
     * future lockouts from their successful logins.
     *
     * @param username the username of the user logged in
     */
    public void loggedIn(String username)
    {
        userLoginTimes.remove(username) ;
    }
}
