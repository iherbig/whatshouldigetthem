package com.project.classes;

import com.project.controllers.DatabaseController;

/**
 * File: WishList.java
 * Description: Handles the saving and retrieval of a user's wishlist from the database.
 *
 * @author Ian Herbig
 */
public class WishList
{
    private String wishList ;
    private String userName ;
    
    public WishList(String wishList, String userName)
    {
        this.wishList = wishList ;
        this.userName = userName ;
    }

    /**
     * Attempts to save the provided wishlist in the database, associating it with the User
     * trying to save the wishlist.
     *
     * @return whether or not the save succeeded
     */
    public boolean saveWishList()
    {   
        return DatabaseController.saveWishList(wishList, userName) ;
    }

    /**
     * Gets the wishlist associated with a specified user, if one exists.
     *
     * @param userName the name of the user who wants their wishlist
     * @return the user's wishlist as a comma-separated list of items
     */
    public static String getWishList(String userName)
    {
        return DatabaseController.retrieveWishList(userName) ;
    }
}
