package com.project.classes;

import com.project.controllers.DatabaseController;
import java.util.ArrayList;

/**
 * File: Item.java
 * Description: An object to represent an Item to be recommended to the user.
 * Contains a constant which is the number of items in the database. Also
 * contains a static helper method to retrieve all items on the database.
 *
 * @author Ian Herbig
 */
public class Item
{
    public static final int NUM_ITEMS = 100 ;

    private String itemName ;
    private Category category ;

    /**
     * Default constructor initializes the instance variables
     * to an invalid value.
     */
    public Item()
    {
        itemName = "" ;
        category = new Category() ;
    }

    /**
     * Initializes the item name to a supplied value, and the
     * category to an invalid value.
     * @param itemName
     */
    public Item(String itemName)
    {
        this.itemName = itemName ;
        category = new Category() ;
    }

    /**
     * Initializes both the item name and category to supplied values.
     *
     * @param itemName the provided item name
     * @param category the item's category
     */
    public Item(String itemName, Category category)
    {
        this.itemName = itemName ;
        this.category = category ;
    }

    /**
     * A simple getter for the item name.
     *
     * @return the Item's name
     */
    public String getName()
    {
        return itemName ;
    }

    /**
     * A simple getter for the Item's Category.
     *
     * @return the Item's Category
     */
    public Category getCategory()
    {
        return category ;
    }

    /**
     * Overridden toString() to produce the Item's name
     * when the Item is used as a String.
     *
     * @return the Item's name
     */
    @Override
    public String toString()
    {
        return itemName ;
    }

    /**
     * A static method to retrieve all Items from the database.
     * Potentially INCREDIBLY inefficient, should not be used
     * with a large database of items.
     *
     * @return an ArrayList of all items in the database
     */
    public static ArrayList<Item> getAllItems()
    {
        return DatabaseController.getAllItems() ;
    }
}
