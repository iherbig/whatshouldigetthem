package com.project.classes;

import com.project.controllers.DatabaseController;
import com.project.interfaces.Recommendation;
import java.util.ArrayList;

/**
 * File: Questionnaire.java
 * Description: Handles the selection of a Category from a provided list of
 * categories generated from the user's responses to questions. A
 * recommendation is generated from them and provided to the caller.
 *
 * @author Ian Herbig
 */
public class Questionnaire implements Recommendation
{
    private ArrayList<Category> userCategories ;
    
    public Questionnaire()
    {
        userCategories = new ArrayList<>() ;
    }

    public Questionnaire(ArrayList<Category> userCategories)
    {
        this.userCategories = userCategories ;
    }

    /**
     * Picks a category from the list of categories representing user's
     * responses to questions and then retrieves an item from the database
     * in that category.
     *
     * @return the Item retrieved from the database
     */
    @Override
    public Item generateRecommendation()
    {
        int [] categoryPoints = new int[Category.NUM_CATEGORIES] ;
        int topCategory = 0 ;
        
        for(Category category : userCategories)
        {
            int categoryNumber = category.getCategoryNumber() ;
            if(categoryNumber == 0)
                continue ;
            
            categoryPoints[categoryNumber - 1]++ ;
            
            if(categoryPoints[categoryNumber - 1] > topCategory)
                topCategory = categoryPoints[categoryNumber - 1] ;
        }
        
        return DatabaseController.getItem(new Category(topCategory)) ;
    }
}
