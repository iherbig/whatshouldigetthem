package com.project.classes;

import com.project.controllers.DatabaseController;
import com.project.interfaces.Recommendation;

/**
 * File: Random.java
 * Description: Randomly gets an Item from the database.
 *
 * @author Ian Herbig
 */
public class Random implements Recommendation
{
    /**
     * Produces an Item from a randomly selected category.
     *
     * @return the randomly selected Item
     */
    @Override
    public Item generateRecommendation()
    {
        Category category = pickCategory() ;
        Item recommendation = DatabaseController.getItem(category) ;
        
        return recommendation ;
    }

    private static Category pickCategory()
    {
        int max = Category.NUM_CATEGORIES ;
        int min = 1 ;
                
        return new Category(randomNumber(min, max)) ;
    }

    /**
     * A helper method to generate a random number within an inclusive range.
     *
     * @param min the lower bound for the random number
     * @param max the upper bound for the random number
     * @return a random number within the inclusive range
     */
    public static int randomNumber(int min, int max)
    {
        java.util.Random rand = new java.util.Random() ;

        return rand.nextInt(max - min + 1) + min ;
    }
}