package com.project.classes;

/**
 * File: Category.java
 * Description: An object to represent a category of items. Contains
 * constants which serve as outside reference to the total number
 * of categories and the total number of items in each category
 * based on those values in the database.
 *
 * @author Ian Herbig
 */
public class Category
{
    public static final int NUM_CATEGORIES = 10 ;
    public static final int NUM_ITEMS_PER_CATEGORY = 10 ;
    
    private int categoryNumber ;

    /**
     * Default constructor initializes the only instance variable to an
     * invalid value ;
     */
    public Category()
    {
        categoryNumber = 0 ;
    }

    /**
     * Constructs a Category with a specified category number.
     *
     * @param categoryNumber the category number specified
     */
    public Category(int categoryNumber)
    {
        this.categoryNumber = categoryNumber ;
    }

    /**
     * A simple getter.
     * @return the Category's number
     */
    public int getCategoryNumber()
    {
        return categoryNumber ;
    }
}
