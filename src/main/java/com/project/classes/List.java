package com.project.classes;

import com.project.controllers.DatabaseController;
import com.project.interfaces.Recommendation;
import java.util.ArrayList;

/**
 * File: List.java
 * Description: Handles the selection of a Category from a provided list of
 * categories that the user selected items from. A recommendation is generated
 * from them and provided to the caller.
 *
 * @author Ian Herbig
 */
public class List implements Recommendation
{
    private ArrayList<Category> categoryList ;

    /**
     * A simple constructor which associates the user-created list
     * of categories with the object.
     *
     * @param categoryList the user-created list of categories
     */
    public List(ArrayList<Category> categoryList)
    {
        this.categoryList = categoryList ;
    }

    /**
     * Picks a category from the list of user-selected categories
     * and then retrieves an item from the database in that category.
     *
     * @return the Item retrieved from the database
     */
    @Override
    public Item generateRecommendation()
    {
        int [] categoryPoints = new int[Category.NUM_CATEGORIES] ;
        int topCategory = 0 ;
        
        for(Category category : categoryList)
        {
            int categoryNumber = category.getCategoryNumber() ;
            categoryPoints[categoryNumber - 1]++ ;
            
            if(categoryPoints[categoryNumber - 1] > topCategory)
                topCategory = categoryNumber ;
        }
        
        return DatabaseController.getItem(new Category(topCategory)) ;
    }
}
