package com.project.launch;

import java.io.File;
import org.apache.catalina.startup.Tomcat;

/**
 * File: Main.java
 * Description: This file starts the server which hosts the web application.
 * Requires the use of Tomcat v. 8.0.3 or better.
 *
 * @author iherb001
 */
public class Main
{
    public static void main(String[] args) throws Exception
    {
        String webappDirLocation = "src/main/webapp/";
        Tomcat tomcat = new Tomcat();

        /*
        * Look for environment variable for port
        * and default to 8080 if it isn't there.
        */
        String webPort = System.getenv("PORT");
        if(webPort == null || webPort.isEmpty())
        {
            webPort = "8080";
        }

        tomcat.setPort(Integer.valueOf(webPort));

        tomcat.addWebapp("/", new File(webappDirLocation).getAbsolutePath());
        System.out.println("configuring app with basedir: " 
                + new File("./" + webappDirLocation).getAbsolutePath());

        tomcat.start();
        tomcat.getServer().await();  
    }
}
