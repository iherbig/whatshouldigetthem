package com.project.connection;

import java.sql.Connection ;
import java.sql.DriverManager ;
import java.sql.SQLException ;

/**
 * File: Database.java
 * Description: This class serves as the code representation of the database and handles
 * the connection thereto.
 *
 * @author Ian Herbig
 */
public class Database
{
    private static final String URL = "" ;
    private static final String USERNAME = "" ;
    private static final String PASSWORD = "" ;
    
    /**
     * The only method is static, no constructor needed.
     */
    private Database(){}
    
    /**
     * This method connects to the database using a JDBC connection.
     * @return A SQL connection to the database.
     */
    public static Connection connect()
    {
        try
        {
            return DriverManager.getConnection(URL, USERNAME, PASSWORD) ;
        }
        catch(SQLException e)
        {
            StringBuilder error = new StringBuilder() ;
            error.append("The connection to the database could not be established.\n"
                    + "SQL State: ") ;
            error.append(e.getSQLState()).append("\n").append(e) ;
            System.out.print(error) ;
        }
        
        return null ;
    }
}
